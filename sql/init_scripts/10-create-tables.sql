DROP TABLE IF EXISTS address CASCADE;
DROP TABLE IF EXISTS country CASCADE;
DROP TABLE IF EXISTS customer CASCADE;
DROP TABLE IF EXISTS address_status CASCADE;
DROP TABLE IF EXISTS customer_address CASCADE;
DROP TABLE IF EXISTS publisher CASCADE;
DROP TABLE IF EXISTS book CASCADE;
DROP TABLE IF EXISTS author CASCADE;
DROP TABLE IF EXISTS book_language CASCADE;
DROP TABLE IF EXISTS shipping_method CASCADE;
DROP TABLE IF EXISTS order_line CASCADE;
DROP TABLE IF EXISTS book_author CASCADE;
DROP TABLE IF EXISTS cust_order CASCADE;
DROP TABLE IF EXISTS order_status CASCADE;
DROP TABLE IF EXISTS order_history CASCADE;

CREATE TABLE IF NOT EXISTS author (
	author_id INT GENERATED ALWAYS AS IDENTITY,
	author_name TEXT NOT NULL,
	PRIMARY KEY(author_id)
);

CREATE TABLE IF NOT EXISTS book_language(
	language_id INT GENERATED ALWAYS AS IDENTITY,
	language_code VARCHAR(7) NOT NULL,
	language_name VARCHAR(30),
	PRIMARY KEY(language_id)
);

CREATE TABLE IF NOT EXISTS publisher (
	publisher_id INT GENERATED ALWAYS AS IDENTITY,
	publisher_name TEXT NOT NULL,
	PRIMARY KEY(publisher_id)
);

CREATE TABLE IF NOT EXISTS address_status(
	status_id INT GENERATED ALWAYS AS IDENTITY,
	address_status TEXT NOT NULL,
	PRIMARY KEY(status_id)
);

CREATE TABLE IF NOT EXISTS order_status(
	status_id INT GENERATED ALWAYS AS IDENTITY,
	status_value TEXT NOT NULL,
	PRIMARY KEY(status_id)
);

CREATE TABLE IF NOT EXISTS shipping_method(
	method_id INT GENERATED ALWAYS AS IDENTITY,
	method_name TEXT NOT NULL,
	cost INT NOT NULL,
	PRIMARY KEY(method_id)
);

CREATE TABLE IF NOT EXISTS country(
	country_id INT GENERATED ALWAYS AS IDENTITY,
	country_name TEXT NOT NULL,
	PRIMARY KEY(country_id)
);

CREATE TABLE IF NOT EXISTS address(
	address_id INT GENERATED ALWAYS AS IDENTITY,
	street_number VARCHAR(30) NOT NULL,
	street_name TEXT NOT NULL,
	city TEXT NOT NULL,
	country_id INT NOT NULL,
	PRIMARY KEY(address_id),
	CONSTRAINT country_id_fk
		FOREIGN KEY(country_id)
			REFERENCES country(country_id)
);

CREATE TABLE IF NOT EXISTS customer(
	customer_id INT GENERATED ALWAYS AS IDENTITY,
	first_name VARCHAR(20) NOT NULL,
	last_name VARCHAR(20) NOT NULL,
	email VARCHAR(40),
	PRIMARY KEY(customer_id)
);

CREATE TABLE IF NOT EXISTS customer_address(
	customer_id INT GENERATED ALWAYS AS IDENTITY,
	address_id INT GENERATED ALWAYS AS IDENTITY,
	status_id INT,
	PRIMARY KEY(customer_id,address_id),
	CONSTRAINT customer_id_fk
		FOREIGN KEY (customer_id)
			REFERENCES customer(customer_id),
	CONSTRAINT address_id_fk
		FOREIGN KEY (address_id)
			REFERENCES address(address_id),
	CONSTRAINT status_id_fk
		FOREIGN KEY (status_id)
			REFERENCES address_status(status_id)
);

CREATE TABLE IF NOT EXISTS book(
	book_id INT GENERATED ALWAYS AS IDENTITY,
	title TEXT NOT NULL,
	isbn13 VARCHAR(20) NOT NULL,
	language_id INT NOT NULL,
	num_pages INT NOT NULL,
	publication_date DATE DEFAULT CURRENT_DATE,
	publisher_id INT NOT NULL,
	PRIMARY KEY(book_id),
	CONSTRAINT language_id_fk
		FOREIGN KEY (language_id)
			REFERENCES book_language(language_id),
	CONSTRAINT publisher_id_fk
		FOREIGN KEY (publisher_id)
			REFERENCES publisher(publisher_id)
);

CREATE TABLE IF NOT EXISTS cust_order(
	order_id INT GENERATED ALWAYS AS IDENTITY,
	order_date TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP, 
	customer_id INT NOT NULL,
	shipping_method_id INT NOT NULL,
	dest_address_id INT NOT NULL,
	PRIMARY KEY(order_id),
	CONSTRAINT customer_id_fk
		FOREIGN KEY (customer_id)
			REFERENCES customer(customer_id),
	CONSTRAINT shipping_method_id_fk
		FOREIGN KEY (shipping_method_id)
			REFERENCES shipping_method(method_id),
	CONSTRAINT dest_address_id_fk
		FOREIGN KEY (dest_address_id)
			REFERENCES address(address_id)
);

CREATE TABLE IF NOT EXISTS book_author(
	book_id INT,
	author_id INT,
	PRIMARY KEY(book_id,author_id),
	CONSTRAINT book_id_fk
		FOREIGN KEY (book_id)
			REFERENCES book(book_id),
	CONSTRAINT author_id_fk
		FOREIGN KEY (author_id)
			REFERENCES author(author_id)
);

CREATE TABLE IF NOT EXISTS order_line(
	line_id INT GENERATED ALWAYS AS IDENTITY,
	order_id INT NOT NULL,
	book_id INT NOT NULL,
	price INT NOT NULL,
	PRIMARY KEY(line_id),
	CONSTRAINT order_id_fk
		FOREIGN KEY (order_id)
			REFERENCES cust_order(order_id),
	CONSTRAINT book_id_fk
		FOREIGN KEY (book_id)
			REFERENCES book(book_id)
);

CREATE TABLE IF NOT EXISTS order_history(
	history_id INT GENERATED ALWAYS AS IDENTITY,
	order_id INT NOT NULL,
	status_id INT NOT NULL,
	status_date DATE DEFAULT CURRENT_DATE,
	PRIMARY KEY(history_id),
	CONSTRAINT order_id_fk
		FOREIGN KEY (order_id)
			REFERENCES cust_order(order_id),
	CONSTRAINT status_id_fk
		FOREIGN KEY (status_id)
			REFERENCES order_status(status_id)
);
